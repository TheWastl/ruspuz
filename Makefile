# Copyright 2020 Wastl
#
# This file is part of RusPuz.
#
# RusPuz is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RusPuz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.

include config.mk

.PHONY : main clean depend .FORCE

.FORCE :

SOURCE_DIR=src
RESOURCE_DIR=resources

main : $(APP_NAME)

$(RESOURCE_DIR)/$(RESOURCE_FILE) : .FORCE
	$(MAKE) -C $(RESOURCE_DIR)

$(SOURCE_DIR)/lib$(APP_NAME).a : .FORCE
	$(MAKE) -C $(SOURCE_DIR)

$(APP_NAME) : $(RESOURCE_DIR)/$(RESOURCE_FILE) $(SOURCE_DIR)/lib$(APP_NAME).a
	$(CC) $(LFLAGS) -o $@ $^ $(LIBS)

depend :
	$(MAKE) -C $(SOURCE_DIR) depend
	$(MAKE) -C $(RESOURCE_DIR) depend

clean :
	$(MAKE) -C $(SOURCE_DIR) clean
	$(MAKE) -C $(RESOURCE_DIR) clean
	rm -f $(APP_NAME)
