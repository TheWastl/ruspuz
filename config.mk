# Copyright 2020 Wastl
#
# This file is part of RusPuz.
#
# RusPuz is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RusPuz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.

APP_NAME=ruspuz

CC=cc
WARNINGS=-std=c99 -Wall -Wpedantic
MACROS=-DAPP_NAME=\"$(APP_NAME)\"
OPTIMIZE=-O3
CLIBS=`pkg-config --cflags gtk+-3.0`
CFLAGS=$(WARNINGS) $(MACROS) $(OPTIMIZE) $(CLIBS)

LIBS=-lm `pkg-config --libs gtk+-3.0`
LFLAGS=

GLIB_MKENUMS=glib-mkenums
GLIB_COMPILE_RESOURCES=glib-compile-resources
GDK_PIXBUF_PIXDATA=gdk-pixbuf-pixdata

RESOURCE_FILE=resources.o
