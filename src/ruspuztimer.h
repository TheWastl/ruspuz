/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _RUSPUZ_TIMER_H
#define _RUSPUZ_TIMER_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define RUSPUZ_TYPE_TIMER (ruspuz_timer_get_type())
G_DECLARE_DERIVABLE_TYPE(RuspuzTimer, ruspuz_timer, RUSPUZ, TIMER, GtkLabel)

struct _RuspuzTimerClass {
	GtkLabelClass parent;

	gpointer padding[16];
};

static inline GtkWidget *ruspuz_timer_new(void) {
	return GTK_WIDGET(g_object_new(RUSPUZ_TYPE_TIMER, NULL));
}

int ruspuz_timer_elapsed(RuspuzTimer *);
void ruspuz_timer_start(RuspuzTimer *);
void ruspuz_timer_stop(RuspuzTimer *);
void ruspuz_timer_continue(RuspuzTimer *);
gboolean ruspuz_timer_running(RuspuzTimer *);

G_END_DECLS

#endif
