/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ruspuz.h"
#include "ruspuzarea.h"

#include <glib/gprintf.h>

#define SHUFFLE_MOVES_PER_TILE 10
#define ANIMATION_DURATION 500000
//#define ANIMATION_DURATION 10000000

#define ARROW_HEAD_SIZE 30

#include <math.h>

typedef struct {
	GdkPixbuf *image;
	cairo_pattern_t *source;

	RuspuzAreaDisplay display;
	gint64 anim_start, anim_time;
	guint tick_id;

	gint width, height;

	cairo_matrix_t main_matrix;

	gint piece_width, piece_height;
	gint pieces_horiz, pieces_vert;

	struct pos arrow;
	struct pos *pos;
} RuspuzAreaPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(RuspuzArea, ruspuz_area, GTK_TYPE_DRAWING_AREA)

enum {
	PROP0,
	PROP_IMAGE,
	PROP_IMAGE_WIDTH,
	PROP_IMAGE_HEIGHT,
	PROP_PIECE_WIDTH,
	PROP_PIECE_HEIGHT,
	PROP_PIECES_HORIZ,
	PROP_PIECES_VERT,
	PROP_DISPLAY,
	N_PROPS
};

enum {
	SIGNAL_SOLVED,
	SIGNAL_ANIM_COMPLETE,
	SIGNAL_MOVE,
	N_SIGNALS
};

/* Opposite directions must be two apart */
enum { DOWN, RIGHT, UP, LEFT };

static const struct pos neighbors[4] = {
	[DOWN] = { 0,  1 }, [RIGHT] = {  1, 0 },
	[UP]   = { 0, -1 },  [LEFT] = { -1, 0 }
};

static GParamSpec *props[N_PROPS] = { NULL };
static guint signals[N_SIGNALS];

static void ruspuz_area_class_init(RuspuzAreaClass *);
static void ruspuz_area_init(RuspuzArea *);
static void ruspuz_area_dispose(GObject *);
static void ruspuz_area_finalize(GObject *);
static void ruspuz_area_set_property(GObject *, guint, const GValue *,
  GParamSpec *);
static void ruspuz_area_get_property(GObject *, guint, GValue *,
  GParamSpec *);

static gboolean ruspuz_area_default_solved(RuspuzArea *);
static void ruspuz_area_nop(RuspuzArea *);
static void ruspuz_area_default_move(RuspuzArea *, gint, gint, gint, gint);

static void ruspuz_update_pieces(RuspuzArea *);
static void ruspuz_update_piece_size(RuspuzArea *);

static void ruspuz_area_realize(GtkWidget *);
static void ruspuz_area_map(GtkWidget *);
static void ruspuz_area_unrealize(GtkWidget *);
static void ruspuz_area_make_pattern(RuspuzArea *);
static inline void generate_numbers(RuspuzArea *);
static gboolean ruspuz_area_draw(GtkWidget *, cairo_t *);
static void ruspuz_area_size_allocate(GtkWidget *, GtkAllocation *);
static GtkSizeRequestMode ruspuz_area_get_request_mode(GtkWidget *);
static void ruspuz_area_get_preferred_width(GtkWidget *, gint *, gint *);
static void ruspuz_area_get_preferred_height(GtkWidget *, gint *, gint *);
static void ruspuz_area_get_preferred_width_for_height(GtkWidget *, gint,
  gint *, gint *);
static void ruspuz_area_get_preferred_height_for_width(GtkWidget *, gint,
  gint *, gint *);

static inline gint random_neighbor(RuspuzAreaPrivate *, gint *, gint *, gint);
static gboolean ruspuz_area_button_press(GtkWidget *, GdkEventButton *);
static gboolean ruspuz_area_key_press(GtkWidget *, GdkEventKey *);
static void ruspuz_area_move(RuspuzArea *, gint, gint, gint, gint);
static gboolean ruspuz_area_anim_update(GtkWidget *, GdkFrameClock *,
  gpointer);

static gboolean ruspuz_area_enter_leave(GtkWidget *, GdkEventCrossing *);
static gboolean ruspuz_area_motion_notify(GtkWidget *, GdkEventMotion *);

static inline gint sign(gint);

#define GETTER(TYPE, NAME, MEMBER) \
	TYPE ruspuz_area_get_##NAME(self) \
		RuspuzArea *self; \
	{ \
		g_return_val_if_fail(RUSPUZ_IS_AREA(self), (TYPE)0); \
		return ((RuspuzAreaPrivate *) \
		  ruspuz_area_get_instance_private(self))->MEMBER; \
	}

#define GETTER2(TYPE, NAME, MEMBER1, MEMBER2) \
	void ruspuz_area_get_##NAME(self, MEMBER1, MEMBER2) \
		RuspuzArea *self; \
		TYPE *MEMBER1, *MEMBER2; \
	{ \
		g_return_if_fail(RUSPUZ_IS_AREA(self)); \
		RuspuzAreaPrivate *priv = \
		  ruspuz_area_get_instance_private(self); \
		if (MEMBER1 != NULL) *MEMBER1 = priv->MEMBER1; \
		if (MEMBER2 != NULL) *MEMBER2 = priv->MEMBER2; \
	}

static inline gint sign(n)
	gint n;
{
	return (n > 0) - (n < 0);
}

static void ruspuz_area_class_init(klass)
	RuspuzAreaClass *klass;
{
	klass->solved = ruspuz_area_default_solved;
	klass->animation_complete = ruspuz_area_nop;
	klass->move = ruspuz_area_default_move;

	GObjectClass *const obj_class = G_OBJECT_CLASS(klass);
	
	obj_class->set_property = ruspuz_area_set_property;
	obj_class->get_property = ruspuz_area_get_property;
	obj_class->dispose = ruspuz_area_dispose;
	obj_class->finalize = ruspuz_area_finalize;

	props[PROP_IMAGE] = g_param_spec_object("image", "Image",
	  "GdkPixbuf containing the image to be displayed", GDK_TYPE_PIXBUF,
	  G_PARAM_READWRITE);
	props[PROP_IMAGE_WIDTH] = g_param_spec_int("image-width",
	  "Image width", "Width of full image", 0, G_MAXINT, 0,
	  G_PARAM_READABLE);
	props[PROP_IMAGE_HEIGHT] = g_param_spec_int("image-height",
	  "Image height", "Height of full image", 0, G_MAXINT, 0,
	  G_PARAM_READABLE);

	props[PROP_PIECE_WIDTH] = g_param_spec_int("piece-width",
	  "Piece width", "The width of individual pieces", 0, G_MAXINT/2, 0,
	  G_PARAM_READABLE);
	props[PROP_PIECE_HEIGHT] = g_param_spec_int("piece-height",
	  "Piece height", "The height of individual pieces", 0, G_MAXINT/2, 0,
	  G_PARAM_READABLE);
	props[PROP_PIECES_HORIZ] = g_param_spec_int("pieces-horiz",
	  "Number of pieces per row", "The number of pieces in a row",
	  2, G_MAXINT, 4, G_PARAM_READWRITE);
	props[PROP_PIECES_VERT] = g_param_spec_int("pieces-vert",
	  "Number of pieces per column", "The number of pieces in a column",
	  2, G_MAXINT, 4, G_PARAM_READWRITE);

	props[PROP_DISPLAY] = g_param_spec_enum("display",
	  "How to display", "How the area should be displayed currently",
	  RUSPUZ_TYPE_AREA_DISPLAY, RUSPUZ_AREA_TILES, G_PARAM_READWRITE);

	g_object_class_install_properties(obj_class, N_PROPS, props);

	signals[SIGNAL_SOLVED] = g_signal_new("solved",
	  RUSPUZ_TYPE_AREA, G_SIGNAL_RUN_LAST,
	  G_STRUCT_OFFSET(RuspuzAreaClass, solved),
	  g_signal_accumulator_true_handled, NULL, NULL, G_TYPE_BOOLEAN, 0);
	signals[SIGNAL_ANIM_COMPLETE] = g_signal_new("animation-complete",
	  RUSPUZ_TYPE_AREA, G_SIGNAL_RUN_LAST,
	  G_STRUCT_OFFSET(RuspuzAreaClass, animation_complete),
	  NULL, NULL, NULL, G_TYPE_NONE, 0);
	signals[SIGNAL_MOVE] = g_signal_new("move",
	  RUSPUZ_TYPE_AREA, G_SIGNAL_RUN_LAST,
	  G_STRUCT_OFFSET(RuspuzAreaClass, move),
	  NULL, NULL, NULL, G_TYPE_NONE, 4,
	    G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);

	GtkWidgetClass *const wdg_class = GTK_WIDGET_CLASS(klass);

	wdg_class->realize = ruspuz_area_realize;
	wdg_class->map = ruspuz_area_map;
	wdg_class->unrealize = ruspuz_area_unrealize;
	wdg_class->draw = ruspuz_area_draw;
	wdg_class->size_allocate = ruspuz_area_size_allocate;
	wdg_class->get_request_mode = ruspuz_area_get_request_mode;
	wdg_class->get_preferred_width = ruspuz_area_get_preferred_width;
	wdg_class->get_preferred_height = ruspuz_area_get_preferred_height;
	wdg_class->get_preferred_width_for_height =
	  ruspuz_area_get_preferred_width_for_height;
	wdg_class->get_preferred_height_for_width =
	  ruspuz_area_get_preferred_height_for_width;

	wdg_class->button_press_event = ruspuz_area_button_press;
	wdg_class->key_press_event = ruspuz_area_key_press;
	wdg_class->motion_notify_event = ruspuz_area_motion_notify;
	wdg_class->enter_notify_event = wdg_class->leave_notify_event =
	  ruspuz_area_enter_leave;
}

static void ruspuz_area_init(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	gtk_widget_set_can_focus(GTK_WIDGET(self), TRUE);
	gtk_widget_set_focus_on_click(GTK_WIDGET(self), TRUE);

	priv->image = NULL;
	priv->source = NULL;
	priv->display = RUSPUZ_AREA_TILES;
	ruspuz_area_set_pieces(self, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	ruspuz_area_set_image(self, NULL);
	priv->arrow = (struct pos) { -1, -1 };
}

static void ruspuz_area_dispose(gobj)
	GObject *gobj;
{
	G_OBJECT_CLASS(ruspuz_area_parent_class)->dispose(gobj);

	GtkWidget *widget = GTK_WIDGET(gobj);

	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(gobj));

	/* Unrealizing will drop the source */
	if (gtk_widget_get_realized(widget))
	  gtk_widget_unrealize(widget);

	g_clear_object(&priv->image);
	priv->piece_width = priv->piece_height = 100;
}

static void ruspuz_area_finalize(gobj)
	GObject *gobj;
{
	g_free(((RuspuzAreaPrivate *)
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(gobj)))->pos);

	G_OBJECT_CLASS(ruspuz_area_parent_class)->finalize(gobj);
}

static void ruspuz_area_set_property(gobj, prop, value, pspec)
	GObject *gobj;
	const guint prop;
	const GValue *const value;
	GParamSpec *const pspec;
{
	RuspuzArea *self = RUSPUZ_AREA(gobj);
	switch (prop) {
		case PROP_IMAGE:
			ruspuz_area_set_image(self, g_value_get_object(value));
			break;

		case PROP_DISPLAY:
			ruspuz_area_set_display(self, g_value_get_enum(value));

		case PROP_PIECES_HORIZ:
			ruspuz_area_set_pieces_horiz(self,
			  g_value_get_int(value));
			break;

		case PROP_PIECES_VERT:
			ruspuz_area_set_pieces_vert(self,
			  g_value_get_int(value));
			break;
	}
}

static void ruspuz_area_get_property(gobj, prop, value, pspec)
	GObject *gobj;
	guint prop;
	GValue *value;
	GParamSpec *pspec;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(gobj));
	switch (prop) {
		case PROP_IMAGE:
			g_value_set_object(value, priv->image);
			break;
		case PROP_IMAGE_WIDTH:
			g_value_set_int(value, priv->width);
			break;
		case PROP_IMAGE_HEIGHT:
			g_value_set_int(value, priv->height);
			break;
		case PROP_DISPLAY:
			g_value_set_enum(value, priv->display);
			break;
		case PROP_PIECE_WIDTH:
			g_value_set_int(value, priv->piece_width);
			break;
		case PROP_PIECE_HEIGHT:
			g_value_set_int(value, priv->piece_height);
			break;
		case PROP_PIECES_HORIZ:
			g_value_set_int(value, priv->pieces_horiz);
			break;
		case PROP_PIECES_VERT:
			g_value_set_int(value, priv->pieces_vert);
			break;
	}
}

void ruspuz_area_set_image(self, image)
	RuspuzArea *self;
	GdkPixbuf *image;
{
	g_return_if_fail(RUSPUZ_IS_AREA(self));

	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	GtkWidget *wdg = GTK_WIDGET(self);

	if (priv->image != NULL) g_object_unref(priv->image);

	GObject *gobj = G_OBJECT(self);

	if (image == NULL) {
		priv->image = NULL;
		priv->piece_width = priv->piece_height = 100;

		g_object_notify_by_pspec(gobj, props[PROP_PIECE_WIDTH]);
		g_object_notify_by_pspec(gobj, props[PROP_PIECE_HEIGHT]);
	} else {
		g_return_if_fail(GDK_IS_PIXBUF(image));

		priv->image = g_object_ref(image);

		priv->width = gdk_pixbuf_get_width(image);
		priv->height = gdk_pixbuf_get_height(image);

		g_object_notify_by_pspec(gobj, props[PROP_IMAGE_WIDTH]);
		g_object_notify_by_pspec(gobj, props[PROP_IMAGE_HEIGHT]);

		if (gtk_widget_get_realized(wdg)) {
			if (priv->source != NULL)
			  cairo_pattern_destroy(priv->source);
			ruspuz_area_make_pattern(self);
		}
	}

	g_object_notify_by_pspec(gobj, props[PROP_IMAGE]);

	ruspuz_update_piece_size(self);

}

GETTER(GdkPixbuf *, image, image)

GETTER2(gint, image_size, width, height)
GETTER(gint, image_width, width)
GETTER(gint, image_height, height)

void ruspuz_area_set_pieces(self, horiz, vert)
	RuspuzArea *self;
	gint horiz, vert;
{
	g_return_if_fail(RUSPUZ_IS_AREA(self));
	g_return_if_fail(horiz >= 2 && vert >= 2);

	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	priv->pieces_horiz = horiz;
	priv->pieces_vert = vert;

	ruspuz_update_pieces(self);
}

GETTER2(gint, pieces, pieces_horiz, pieces_vert)

void ruspuz_area_set_pieces_horiz(self, horiz)
	RuspuzArea *self;
	gint horiz;
{
	g_return_if_fail(RUSPUZ_IS_AREA(self));
	g_return_if_fail(horiz >= 2);

	((RuspuzAreaPrivate *)ruspuz_area_get_instance_private(self))
	  ->pieces_horiz = horiz;

	ruspuz_update_pieces(self);
}

GETTER(gint, pieces_horiz, pieces_horiz)

void ruspuz_area_set_pieces_vert(self, vert)
	RuspuzArea *self;
	gint vert;
{
	g_return_if_fail(RUSPUZ_IS_AREA(self));
	g_return_if_fail(vert >= 2);

	((RuspuzAreaPrivate *)ruspuz_area_get_instance_private(self))
	  ->pieces_vert = vert;

	ruspuz_update_pieces(self);
}

GETTER(gint, pieces_vert, pieces_vert)

static void ruspuz_update_pieces(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	g_free(priv->pos);
	priv->pos = g_new(struct pos, priv->pieces_horiz * priv->pieces_vert);
	ruspuz_area_reset(self);

	GObject *gobj = G_OBJECT(self);
	g_object_notify_by_pspec(gobj, props[PROP_PIECES_HORIZ]);
	g_object_notify_by_pspec(gobj, props[PROP_PIECES_VERT]);

	ruspuz_update_piece_size(self);
}

static void ruspuz_update_piece_size(self)
	RuspuzArea *self;
{
	GObject *gobj = G_OBJECT(self);
	GtkWidget *widget = GTK_WIDGET(self);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	if (priv->image == NULL) {
		priv->width = priv->piece_width * priv->pieces_horiz;
		priv->height = priv->piece_height * priv->pieces_vert;

		g_object_notify_by_pspec(gobj, props[PROP_IMAGE_WIDTH]);
		g_object_notify_by_pspec(gobj, props[PROP_IMAGE_HEIGHT]);

		if (gtk_widget_get_realized(widget)) {
			if (priv->source != NULL)
			  cairo_pattern_destroy(priv->source);
			ruspuz_area_make_pattern(self);
		}
	} else {
		priv->piece_width = priv->width / priv->pieces_horiz;
		priv->piece_height = priv->height / priv->pieces_vert;

		g_object_notify_by_pspec(gobj, props[PROP_PIECE_WIDTH]);
		g_object_notify_by_pspec(gobj, props[PROP_PIECE_HEIGHT]);
	}

	if (gtk_widget_get_realized(widget)) {
		gtk_widget_queue_resize(widget);
		gtk_widget_queue_draw(widget);
	}
}

GETTER2(gint, piece_size, piece_width, piece_height)
GETTER(gint, piece_width, piece_width)
GETTER(gint, piece_height, piece_height)

void ruspuz_area_set_display(self, display)
	RuspuzArea *self;
	RuspuzAreaDisplay display;
{
	g_return_if_fail(RUSPUZ_IS_AREA(self));

	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	GtkWidget *widget = GTK_WIDGET(self);

	if (display == priv->display) return;
	if (display == RUSPUZ_AREA_ANIM) {
		priv->tick_id = gtk_widget_add_tick_callback(widget,
		  ruspuz_area_anim_update, NULL, NULL);
		if (gtk_widget_get_mapped(widget))
		  priv->anim_start = gdk_frame_clock_get_frame_time(
		    gtk_widget_get_frame_clock(widget)
		  );
		priv->anim_time = 0;
	} else if (priv->display == RUSPUZ_AREA_ANIM)
	  gtk_widget_remove_tick_callback(widget, priv->tick_id);
	priv->display = display;
	gtk_widget_queue_draw(widget);
}

GETTER(RuspuzAreaDisplay, display, display)

void ruspuz_area_reset(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	gint i = priv->pieces_vert * priv->pieces_horiz - 1;

	priv->pos[i].x = priv->pos[i].y = -1;
	while (--i >= 0) {
		priv->pos[i].x = i % priv->pieces_horiz;
		priv->pos[i].y = i / priv->pieces_horiz;
	}

	if (gtk_widget_get_realized(GTK_WIDGET(self)))
	  gtk_widget_queue_draw(GTK_WIDGET(self));
}

static inline gint random_neighbor(priv, x, y, avoid)
	RuspuzAreaPrivate *priv;
	gint *x, *y, avoid;
{
	struct { gint x, y, idx; } ok[4];
	gint i = 0;
	for (gint c = 0; c < 4; c++) {
		if (c == avoid) continue;
		ok[i].x = *x + neighbors[c].x;
		if (ok[i].x < 0 || ok[i].x >= priv->pieces_horiz) continue;
		ok[i].y = *y + neighbors[c].y;
		if (ok[i].y < 0 || ok[i].y >= priv->pieces_vert) continue;
		ok[i++].idx = c;
	}
	i = g_random_int_range(0, i);
	*x = ok[i].x;
	*y = ok[i].y;
	return ok[i].idx ^ 2;
}

void ruspuz_area_shuffle(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	/* We use a parity algorithm to make sure that the resulting puzzle is
	 * always solvable.
	 * https://en.wikipedia.org/wiki/15_puzzle
	 * https://en.wikipedia.org/wiki/Parity_of_a_permutation
	 */

	gsize parity = 0;
	gsize size = priv->pieces_horiz * priv->pieces_vert;
	for (gsize i = 0; i < size; i++) {
		gsize swap = g_random_int_range(i, size);
		if (swap != i) {
			struct pos tmp = priv->pos[i];
			priv->pos[i] = priv->pos[swap];
			priv->pos[swap] = tmp;
			parity ^= 1;
			if (priv->pos[i].x == -1 || priv->pos[swap].x == -1) {
				/* Record walking distance parity */
				parity ^=
				  (swap / priv->pieces_horiz) ^
				     (i / priv->pieces_horiz) ^
				  (swap % priv->pieces_horiz) ^
				     (i % priv->pieces_horiz);
			}
		}
	}
	if (parity & 1) {
		/* Odd parity: The puzzle is not solvable. We fix it by
		 * swapping the last two non-empty tiles.
		 */

		gsize swap[2];
		for (gsize i = 0; i < 2; i++) {
			if (priv->pos[--size].x == -1) size--;
			swap[i] = size;
		}

		struct pos tmp = priv->pos[swap[0]];
		priv->pos[swap[0]] = priv->pos[swap[1]];
		priv->pos[swap[1]] = tmp;
	}

	GtkWidget *widget = GTK_WIDGET(self);

	if (gtk_widget_get_realized(widget)) gtk_widget_queue_draw(widget);
}

gboolean ruspuz_area_point_to_pos(self, x, y, pos)
	RuspuzArea *self;
	gdouble x, y;
	struct pos *pos;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	cairo_matrix_t mx = priv->main_matrix;
	cairo_matrix_invert(&mx);
	cairo_matrix_transform_point(&mx, &x, &y);

	if (x < 0 || y < 0) return FALSE;

	pos->x = x / priv->piece_width;
	if (pos->x >= priv->pieces_horiz) return FALSE;

	pos->y = y / priv->piece_height;

	return pos->y < priv->pieces_vert;
}

static void ruspuz_area_realize(widget)
	GtkWidget *widget;
{
	RuspuzArea *self = RUSPUZ_AREA(widget);

	GTK_WIDGET_CLASS(ruspuz_area_parent_class)->realize(widget);

	gtk_widget_add_events(widget,
	  GDK_BUTTON_PRESS_MASK | GDK_KEY_PRESS_MASK |
	  GDK_POINTER_MOTION_MASK | GDK_ENTER_NOTIFY_MASK |
	    GDK_LEAVE_NOTIFY_MASK);
	ruspuz_area_make_pattern(self);
}

static void ruspuz_area_map(widget)
	GtkWidget *widget;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));

	GTK_WIDGET_CLASS(ruspuz_area_parent_class)->map(widget);

	priv->anim_start =
	  gdk_frame_clock_get_frame_time(gtk_widget_get_frame_clock(widget))
	  + priv->anim_time;
	gtk_widget_grab_focus(widget);
}

static void ruspuz_area_unrealize(widget)
	GtkWidget *widget;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));

	GTK_WIDGET_CLASS(ruspuz_area_parent_class)->unrealize(widget);

	if (priv->source != NULL) {
		cairo_pattern_destroy(priv->source);
		priv->source = NULL;
	}
}

static void ruspuz_area_make_pattern(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	if (priv->image == NULL) {
		generate_numbers(self);
		return;
	}

	cairo_surface_t *surface =
	  gdk_cairo_surface_create_from_pixbuf(priv->image, 0,
	    gtk_widget_get_window(GTK_WIDGET(self)));

	priv->source = cairo_pattern_create_for_surface(surface);

	cairo_surface_destroy(surface);
}

static inline void generate_numbers(self)
	RuspuzArea *self;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	cairo_surface_t *surface = gdk_window_create_similar_surface(
	  gtk_widget_get_window(GTK_WIDGET(self)), CAIRO_CONTENT_ALPHA,
	  100 * priv->pieces_horiz, 100 * priv->pieces_vert);
	cairo_t *cr = cairo_create(surface);

	gint i = 2, j = 1, bound = 10,
	  max = priv->pieces_horiz * priv->pieces_vert;

	cairo_set_font_size(cr, 100.);
	cairo_set_source_rgba(cr, 0., 0., 0., 1.);

	cairo_font_extents_t font_ext;
	cairo_font_extents(cr, &font_ext);

	gdouble scale1 = 10000. / font_ext.height;

	while (j <= max) {
		gchar num[i];
		gdouble //min_above =  G_MAXDOUBLE,
		        //max_below = -G_MAXDOUBLE,
		        max_width =  0;
		if (bound > max) bound = max + 1;

		for (gint c = j; c < bound; c++) {
			cairo_text_extents_t ext;
			if (g_snprintf(num, i, "%d", c) >= i)
			  g_error("Number %d not fitting into string of "
			    "length %d", c, i);
			cairo_text_extents(cr, num, &ext);

			//if (min_above > ext.y_bearing)
			//  min_above = ext.y_bearing;
			//if (max_below < ext.y_bearing + ext.height)
			//  max_below = ext.y_bearing + ext.height;
			if (max_width < ext.width)
			  max_width = ext.width;
		}

		gdouble scale2 = 8000. / max_width;
		gdouble scale = MIN(scale1, scale2);

		cairo_set_font_size(cr, scale);

		cairo_font_extents(cr, &font_ext);

		for (; j < bound; j += 2) {
			cairo_text_extents_t ext;
			g_snprintf(num, i, "%d", j);
			cairo_text_extents(cr, num, &ext);

			j--;

			cairo_move_to(cr,
			  100. * (j % priv->pieces_horiz)
			    + (100. - ext.width) / 2. - ext.x_bearing,
			  100. * (j / priv->pieces_horiz)
			    + (100. + font_ext.ascent - font_ext.descent) / 2);

			gdouble x, y, r, g, b, a;
			cairo_get_current_point(cr, &x, &y);
			cairo_pattern_get_rgba(cairo_get_source(cr),
			  &r, &g, &b, &a);

			cairo_text_path(cr, num);
		}

		cairo_set_font_size(cr, 100.);

		i++;
		bound *= 10;
	}

	cairo_fill(cr);
	cairo_destroy(cr);

	priv->source = cairo_pattern_create_for_surface(surface);

	cairo_surface_destroy(surface);
}

static gboolean ruspuz_area_button_press(widget, evt)
	GtkWidget *widget;
	GdkEventButton *evt;
{
	if (evt->type != GDK_BUTTON_PRESS || evt->button != 1) return FALSE;

	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	if (priv->display != RUSPUZ_AREA_TILES) return FALSE;

	struct pos pos;

	if (!ruspuz_area_point_to_pos(self, evt->x, evt->y, &pos))
	  return FALSE;

	if (priv->pos[priv->pieces_horiz * pos.y + pos.x].x == -1) return TRUE;

	for (gsize c = 0; c < 4; c++) {
		gint x2 = pos.x + neighbors[c].x;
		if (x2 < 0 || x2 >= priv->pieces_horiz) continue;
		gint y2 = pos.y + neighbors[c].y;
		if (y2 < 0 || y2 >= priv->pieces_vert ||
		  priv->pos[priv->pieces_horiz * y2 + x2].x != -1) continue;
		ruspuz_area_move(self, pos.x, pos.y, x2, y2);
		break;
	}
	return TRUE;
}

static gboolean ruspuz_area_key_press(widget, evt)
	GtkWidget *widget;
	GdkEventKey *evt;
{

	if (evt->type != GDK_KEY_PRESS) return FALSE;
	if (evt->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) return FALSE;

	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	if (priv->display != RUSPUZ_AREA_TILES) return FALSE;

	gsize dir;

	/* Always record the opposite, because that's where we'll look for
	 * the tile to move (from the empty one)
	 */
	switch (evt->keyval) {
		case GDK_KEY_Up:
		case GDK_KEY_w:
			dir = DOWN;
			break;
		case GDK_KEY_Left:
		case GDK_KEY_a:
			dir = RIGHT;
			break;
		case GDK_KEY_Down:
		case GDK_KEY_s:
			dir = UP;
			break;
		case GDK_KEY_Right:
		case GDK_KEY_d:
			dir = LEFT;
			break;
		default:
			return FALSE;
	}

	gint i2 = priv->pieces_horiz * priv->pieces_vert;

	while (--i2 >= 0) {
		if (priv->pos[i2].x == -1) goto found;
	}

	g_error("Can't find empty tile");

found:;
	gint x2 = i2 % priv->pieces_horiz,
	     y2 = i2 / priv->pieces_horiz;

	gint x1 = x2 + neighbors[dir].x;
	if (x1 < 0 || x1 >= priv->pieces_horiz) return TRUE;
	gint y1 = y2 + neighbors[dir].y;
	if (y1 >= 0 && y1 < priv->pieces_vert)
	  ruspuz_area_move(self, x1, y1, x2, y2);


	return TRUE;
}

static void ruspuz_area_move(self, x1, y1, x2, y2)
	RuspuzArea *self;
	gint x1, y1, x2, y2;
{
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	{
		GtkWidget *widget = GTK_WIDGET(self);

		gint i1 = priv->pieces_horiz * y1 + x1,
		     i2 = priv->pieces_horiz * y2 + x2;

		priv->pos[i2] = priv->pos[i1];
		priv->pos[i1].x = priv->pos[i1].y = -1;

		gtk_widget_queue_draw(widget);

		g_signal_emit(self, signals[SIGNAL_MOVE], 0, x1, y1, x2, y2);
	}
	{

		gint i = priv->pieces_horiz * priv->pieces_vert - 1;
		if (priv->pos[i].x != -1) return;
		while (--i >= 0) {
			if (priv->pos[i].x != i % priv->pieces_horiz ||
			  priv->pos[i].y != i / priv->pieces_horiz) return;
		}

		GValue ret = G_VALUE_INIT;
		g_signal_emit(self, signals[SIGNAL_SOLVED], 0, &ret);
	}
}

static gboolean ruspuz_area_motion_notify(widget, event)
	GtkWidget *widget;
	GdkEventMotion *event;
{
	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	if (!ruspuz_area_point_to_pos(self, event->x, event->y, &priv->arrow))
	  priv->arrow = (struct pos) { -1, -1 };

	gtk_widget_queue_draw(widget);

	return TRUE;
}

static gboolean ruspuz_area_enter_leave(widget, event)
	GtkWidget *widget;
	GdkEventCrossing *event;
{
	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	if (!ruspuz_area_point_to_pos(self, event->x, event->y, &priv->arrow))
	  priv->arrow = (struct pos) { -1, -1 };

	gtk_widget_queue_draw(widget);

	return TRUE;
}

static gboolean ruspuz_area_anim_update(widget, clock, _)
	GtkWidget *widget;
	GdkFrameClock *clock;
	gpointer _;
{
	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);

	gtk_widget_queue_draw(widget);

	priv->anim_time =
	  gdk_frame_clock_get_frame_time(clock) - priv->anim_start;
	if (priv->anim_time >= ANIMATION_DURATION) {
		priv->display = RUSPUZ_AREA_IMAGE;
		g_signal_emit(self, signals[SIGNAL_ANIM_COMPLETE], 0);
		return G_SOURCE_REMOVE;
	} else {
		return G_SOURCE_CONTINUE;
	}
}

static gboolean ruspuz_area_draw(widget, cr)
	GtkWidget *widget;
	cairo_t *cr;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));

	cairo_matrix_t mx;

	cairo_transform(cr, &priv->main_matrix);

	if (priv->display != RUSPUZ_AREA_TILES) {
		cairo_matrix_init_identity(&mx);
		cairo_pattern_set_matrix(priv->source, &mx);
		cairo_set_source(cr, priv->source);

		if (priv->display == RUSPUZ_AREA_IMAGE) {
			cairo_paint(cr);
			return TRUE;
		}
		if (priv->display == RUSPUZ_AREA_ANIM) {
			cairo_move_to(cr, 0, 0);
			cairo_line_to(cr,
			  3 * priv->width * priv->anim_time /
			     ANIMATION_DURATION, 0);
			cairo_line_to(cr, 0,
			  3 * priv->height * priv->anim_time / 2 /
			     ANIMATION_DURATION);
			cairo_close_path(cr);
			cairo_fill_preserve(cr);
			cairo_rectangle(cr, 0, 0, priv->width, priv->height);
			cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);
			cairo_clip(cr);
			cairo_set_fill_rule(cr, CAIRO_FILL_RULE_WINDING);
		}
	}

	for (gint x = 0; x < priv->pieces_horiz; x++) {
		for (gint y = 0; y < priv->pieces_vert; y++) {
			gint i = priv->pieces_horiz * y + x;
			cairo_rectangle(cr,
			  x * priv->piece_width, y * priv->piece_height,
			      priv->piece_width,     priv->piece_height);
			if (priv->pos[i].x == -1) {
				cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
				cairo_fill(cr);
				continue;
			}

			cairo_matrix_init_translate(&mx,
			  (priv->pos[i].x - x) * priv->piece_width,
			  (priv->pos[i].y - y) * priv->piece_height);
			cairo_pattern_set_matrix(priv->source, &mx);
			cairo_set_source(cr, priv->source);
			cairo_fill_preserve(cr);
			cairo_set_source_rgb(cr, 0., 0., 0.);
			cairo_stroke(cr);

#if 0
			cairo_set_operator(cr, CAIRO_OPERATOR_HSL_LUMINOSITY);

			cairo_move_to(cr, x * priv->piece_width,
			                  y * priv->piece_height);
			cairo_rel_line_to(cr, BORDER_WIDTH, BORDER_WIDTH);
			cairo_rel_line_to(cr,
			  0, priv->piece_height - 2 * BORDER_WIDTH);
			cairo_rel_line_to(cr, -BORDER_WIDTH, BORDER_WIDTH);
			cairo_close_path(cr);
			cairo_set_source_rgb(cr, 0.9, 0.9, 0.9);
			cairo_fill(cr);

			cairo_move_to(cr, x * priv->piece_width,
			                  y * priv->piece_height);
			cairo_rel_line_to(cr, BORDER_WIDTH, BORDER_WIDTH);
			cairo_rel_line_to(cr,
			  priv->piece_width - 2 * BORDER_WIDTH, 0);
			cairo_rel_line_to(cr, BORDER_WIDTH, -BORDER_WIDTH);
			cairo_close_path(cr);
			cairo_set_source_rgb(cr, 0.95, 0.95, 0.95);
			cairo_fill(cr);

			cairo_move_to(cr, (x+1) * priv->piece_width,
			                  y * priv->piece_height);
			cairo_rel_line_to(cr, -BORDER_WIDTH, BORDER_WIDTH);
			cairo_rel_line_to(cr,
			  0, priv->piece_height - 2 * BORDER_WIDTH);
			cairo_rel_line_to(cr, BORDER_WIDTH, BORDER_WIDTH);
			cairo_close_path(cr);
			cairo_set_source_rgb(cr, 0.8, 0.8, 0.8);
			cairo_fill(cr);

			cairo_move_to(cr, x * priv->piece_width,
			                  (y+1) * priv->piece_height);
			cairo_rel_line_to(cr, BORDER_WIDTH, -BORDER_WIDTH);
			cairo_rel_line_to(cr,
			  priv->piece_width - 2 * BORDER_WIDTH, 0);
			cairo_rel_line_to(cr, BORDER_WIDTH, BORDER_WIDTH);
			cairo_close_path(cr);
			cairo_set_source_rgb(cr, 0.85, 0.85, 0.85);
			cairo_fill(cr);

			cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
#endif
		}
	}

	if (priv->arrow.x != -1) {
		gint i = priv->pieces_horiz * priv->arrow.y + priv->arrow.x;
		if (priv->pos[i].x == priv->arrow.x &&
		    priv->pos[i].y == priv->arrow.y) {
			cairo_set_source_rgba(cr, 0., 1., 0., .3);
			cairo_rectangle(cr,
			  priv->arrow.x * priv->piece_width,
			    priv->arrow.y * priv->piece_height,
			  priv->piece_width,
			    priv->piece_height);
			cairo_fill(cr);
		} else if (priv->pos[i].x != -1) {
			cairo_set_source_rgba(cr, 1., 0., 0., .5);
			cairo_set_line_width(cr, 10.);
			cairo_move_to(cr,
			  priv->arrow.x * priv->piece_width
			  + priv->piece_width / 2,
			  priv->arrow.y * priv->piece_height
			  + priv->piece_height / 2);
			cairo_line_to(cr,
			  priv->pos[i].x * priv->piece_width
			  + priv->piece_width / 2,
			  priv->pos[i].y * priv->piece_height
			  + priv->piece_height / 2);
			gint dx = priv->piece_width *
			           (priv->pos[i].x - priv->arrow.x),
			     dy = priv->piece_height *
			           (priv->pos[i].y - priv->arrow.y);
			gdouble d = sqrt(dx*dx + dy*dy);
			cairo_rel_move_to(cr,
			    (dy - dx) * ARROW_HEAD_SIZE / d,
			  - (dy + dx) * ARROW_HEAD_SIZE / d);
			cairo_rel_line_to(cr,
			    (dx - dy) * ARROW_HEAD_SIZE / d,
			    (dx + dy) * ARROW_HEAD_SIZE / d);
			cairo_rel_line_to(cr,
			  - (dx + dy) * ARROW_HEAD_SIZE / d,
			    (dx - dy) * ARROW_HEAD_SIZE / d);
			cairo_stroke(cr);
		}
	}

	return TRUE;
}

static void ruspuz_area_size_allocate(widget, alloc)
	GtkWidget *widget;
	GtkAllocation *alloc;
{
	RuspuzArea *self = RUSPUZ_AREA(widget);
	RuspuzAreaPrivate *priv = ruspuz_area_get_instance_private(self);
	gdouble scale1 = (gdouble)alloc->width / priv->width;
	gdouble scale2 = (gdouble)alloc->height / priv->height;

	if (scale1 > scale2) {
		cairo_matrix_init_translate(&priv->main_matrix,
		  (alloc->width - priv->width * scale2) / 2, 0);
		cairo_matrix_scale(&priv->main_matrix, scale2, scale2);
	} else {
		cairo_matrix_init_translate(&priv->main_matrix,
		  0, (alloc->height - priv->height * scale1) / 2);
		cairo_matrix_scale(&priv->main_matrix, scale1, scale1);
	}

	gtk_widget_queue_draw(widget);

	GTK_WIDGET_CLASS(ruspuz_area_parent_class)->
	  size_allocate(widget, alloc);
}

static GtkSizeRequestMode ruspuz_area_get_request_mode(widget)
	GtkWidget *widget;
{
	return GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;
}

static void ruspuz_area_get_preferred_width(widget, minimum, natural)
	GtkWidget *widget;
	gint *minimum, *natural;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));
	*minimum = priv->pieces_horiz;
	*natural = priv->width;
}

static void ruspuz_area_get_preferred_height(widget, minimum, natural)
	GtkWidget *widget;
	gint *minimum, *natural;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));
	*minimum = priv->pieces_vert;
	*natural = priv->height;
}

static void ruspuz_area_get_preferred_width_for_height(widget, h, min, nat)
	GtkWidget *widget;
	gint h, *min, *nat;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));
	*min = priv->pieces_horiz;
	*nat = h * priv->width / priv->height;
}

static void ruspuz_area_get_preferred_height_for_width(widget, w, min, nat)
	GtkWidget *widget;
	gint w, *min, *nat;
{
	RuspuzAreaPrivate *priv =
	  ruspuz_area_get_instance_private(RUSPUZ_AREA(widget));
	*min = priv->pieces_vert;
	*nat = w * priv->height / priv->width;
}

static gboolean ruspuz_area_default_solved(self)
	RuspuzArea *self;
{
	ruspuz_area_set_display(self, RUSPUZ_AREA_ANIM);
	//ruspuz_area_set_display(self, RUSPUZ_AREA_IMAGE);

	return TRUE;
}

static void ruspuz_area_nop(self)
	RuspuzArea *self;
{
}

static void ruspuz_area_default_move(self, x1, y1, x2, y2)
	RuspuzArea *self;
	gint x1, y1, x2, y2;
{
}
