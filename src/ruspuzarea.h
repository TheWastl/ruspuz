/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _RUSPUZ_AREA_H
#define _RUSPUZ_AREA_H

//#include <gobject/gobject.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define RUSPUZ_TYPE_AREA ruspuz_area_get_type()

G_DECLARE_DERIVABLE_TYPE(RuspuzArea, ruspuz_area, RUSPUZ, AREA, GtkDrawingArea)

struct _RuspuzAreaClass {
	GtkDrawingAreaClass parent;

	gboolean (*solved)(RuspuzArea *);
	void (*animation_complete)(RuspuzArea *);
	void (*move)(RuspuzArea *, gint, gint, gint, gint);

	gpointer padding[13];
};

typedef enum {
	RUSPUZ_AREA_TILES,
	RUSPUZ_AREA_ANIM,
	RUSPUZ_AREA_IMAGE,
} RuspuzAreaDisplay;

#include "ruspuz.h"
#include "ruspuzarea-enums.h"

static inline GtkWidget *ruspuz_area_new_default(void) {
	return GTK_WIDGET(g_object_new(RUSPUZ_TYPE_AREA, NULL));
}

static inline GtkWidget *ruspuz_area_new(GdkPixbuf *img, gint w, gint h) {
	return GTK_WIDGET(g_object_new(RUSPUZ_TYPE_AREA,
		"image", img,
		"pieces-horiz", w,
		"pieces-vert", h,
		NULL
	));
}

#define GETTER(TYPE, NAME) \
	TYPE ruspuz_area_get_##NAME(RuspuzArea *);
#define GETTER2(TYPE, NAME) \
	void ruspuz_area_get_##NAME(RuspuzArea *, TYPE *, TYPE *);
#define SETTER(TYPE, NAME) \
	void ruspuz_area_set_##NAME(RuspuzArea *, TYPE);
#define SETTER2(TYPE, NAME) \
	void ruspuz_area_set_##NAME(RuspuzArea *, TYPE, TYPE);

#define SETGET(T, N) SETTER(T, N) GETTER(T, N)
#define SETGET2(T, N) SETTER2(T, N) GETTER2(T, N)

SETGET(GdkPixbuf *, image)
GETTER2(gint, image_size)
GETTER(gint, image_width)
GETTER(gint, image_height)

GETTER(gdouble, scale)

SETGET2(gint, pieces)
SETGET(gint, pieces_horiz)
SETGET(gint, pieces_vert)

GETTER2(gint, piece_size)
GETTER(gint, piece_width)
GETTER(gint, piece_height)

SETGET(RuspuzAreaDisplay, display)

#undef GETTER
#undef GETTER2
#undef SETTER
#undef SETTER2

void ruspuz_area_reset(RuspuzArea *);
void ruspuz_area_shuffle(RuspuzArea *);
gboolean ruspuz_area_point_to_pos(RuspuzArea *,
  gdouble, gdouble, struct pos *);

G_END_DECLS

#endif
