/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _RUSPUZ_H
#define _RUSPUZ_H

#include <gtk/gtk.h>

#define PREFIX(SEP) "io" SEP "gitlab" SEP "thewastl" SEP APP_NAME
#define RESOURCE(F) "/" PREFIX("/") "/" F

#define DEFAULT_WIDTH 4
#define DEFAULT_HEIGHT DEFAULT_WIDTH

struct pos { gint x, y; };

#endif
