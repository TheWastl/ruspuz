/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

/*** BEGIN file-header ***/

#include <glib.h>

/*** END file-header ***/
/*** BEGIN file-production ***/

#include "@filename@"

/*** END file-production ***/
/*** BEGIN value-header ***/

GType @enum_name@_get_type()
{
	static volatile size_t t_vol = 0;
	if (g_once_init_enter(&t_vol)) {
		static const G@Type@Value values[] = {
/*** END value-header ***/
/*** BEGIN value-production ***/
			{ @VALUENAME@, "@VALUENAME@", "@valuenick@" },
/*** END value-production ***/
/*** BEGIN value-tail ***/
			{ 0, NULL, NULL }
		};
		g_once_init_leave(&t_vol,
		  g_@type@_register_static("@EnumName@", values));
	}
	return t_vol;
}

/*** END value-tail ***/
