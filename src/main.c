/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ruspuz.h"
#include "ruspuzarea.h"
#include "ruspuztimer.h"

#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <stdlib.h>

/* Struct: state
 *
 * UI state, not state of the game itself (that is handled by <RuspuzArea>)
 */
struct state {
	GtkLabel *moves_label;
	gint moves_count;
	GtkWidget *window;
	RuspuzTimer *timer;
	RuspuzArea *area;
	GtkToggleButton *menu_button;
	GSimpleAction *pause, *cont;
	gulong draw_pause_id, click_pause_id;
};

static gint gmain(GtkApplication *, GApplicationCommandLine *, gpointer);
static void display_window(GtkApplication *, GdkPixbuf *, gint, gint);
static gboolean get_dimension(GVariantDict *, gint *, const gchar *);

static void move(RuspuzArea *, gint, gint, gint, gint, struct state *);
static gboolean solved(RuspuzArea *, struct state *);
static gboolean area_key_press(RuspuzArea *, GdkEventKey *, struct state *);
static gboolean key_press(GtkWidget *, GdkEventKey *, struct state *);
static gboolean draw_pause(GtkWidget *, cairo_t *, struct state *);
static void shuffle_action(GSimpleAction *, GVariant *, gpointer);
static void close_action(GSimpleAction *, GVariant *, gpointer);
static void toggle_pause_action(GSimpleAction *, GVariant *, gpointer);
static void open_action(GSimpleAction *, GVariant *, gpointer);
static void dialog_open(struct state *);
static void open(GtkFileChooserDialog *, gint, gpointer);
static void about_action(GSimpleAction *, GVariant *, gpointer);

static void shuffle(struct state *);
static gboolean toggle_pause(struct state *);

static void startup(GtkApplication *, gpointer);

static GList *icon_list;
static GtkStyleProvider *style;

/* Function: main
 *
 * Builds and runs the GtkApplication:
 * - instantiate
 * - configure options
 * - connect signals
 * - run
 *
 * Parameters:
 *	argc - Number of arguments
 *	argv - Individual arguments
 */
int main(argc, argv)
	int argc;
	char **argv;
{
	GtkApplication *app;
	int status;

	app = gtk_application_new(PREFIX("."),
	  G_APPLICATION_HANDLES_COMMAND_LINE);
	GApplication *gapp = G_APPLICATION(app);

	g_application_add_main_option(gapp, "width", 'w',
	  G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
	  "Number of horizontal tiles", "WIDTH");
	g_application_add_main_option(gapp, "height", 'h',
	  G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
	  "Number of vertical tiles", "HEIGHT");
	g_application_set_option_context_parameter_string(gapp, "FILES");

	g_signal_connect(app, "startup", G_CALLBACK(startup), NULL);
	g_signal_connect(app, "command-line", G_CALLBACK(gmain), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);
	return status;
}

static gboolean get_dimension(opts, p, id)
	GVariantDict *opts;
	gint *p;
	const gchar *id;
{
	if (g_variant_dict_lookup(opts, id, "i", p) && *p < 2) {
		fprintf(stderr, "Illegal %s: must be at least 2\n",
		  id);
		return TRUE;
	} else return FALSE;
}

static gint gmain(app, cmd, _)
	GtkApplication *app;
	GApplicationCommandLine *cmd;
	gpointer _;
{
	gint width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT;
	GVariantDict *opts = g_application_command_line_get_options_dict(cmd);
	if (get_dimension(opts, &width, "width") ||
	    get_dimension(opts, &height, "height")) return 1;

	gchar **args = g_application_command_line_get_arguments(cmd, NULL);
	gint exit_code = 1;

	if (args[1] == NULL) {
		display_window(app, NULL, width, height);
		exit_code = 0;
	} else for (gchar **arg = args + 1; *arg != NULL; arg++) {
		GError *error = NULL;
		GdkPixbuf *image = gdk_pixbuf_new_from_file(*arg, &error);
		if (image == NULL) {
			fputs(error->message, stderr);
			fputc('\n', stderr);
			g_clear_error(&error);
		} else {
			display_window(app, image, width, height);
			g_object_unref(image);
			exit_code = 0;
		}
	}
	g_strfreev(args);
	return exit_code;
}

static void display_window(app, image, width, height)
	GtkApplication *app;
	GdkPixbuf *image;
	gint width, height;
{
	RUSPUZ_TYPE_TIMER;
	RUSPUZ_TYPE_AREA;

	GtkBuilder *builder = gtk_builder_new();
	GError *err = NULL;
	gtk_builder_expose_object(builder, "app", G_OBJECT(app));
	gtk_builder_add_callback_symbols(builder,
		"key-press", G_CALLBACK(key_press),
		"solved", G_CALLBACK(solved),
		"move", G_CALLBACK(move),
		"area-key-press", G_CALLBACK(area_key_press),
		"cleanup", G_CALLBACK(g_free),
		NULL
	);

	if (!gtk_builder_add_from_resource(builder, RESOURCE("ruspuz.ui"),
	  &err)) g_error("%s", err->message);

	struct state *s = g_new(struct state, 1);

	gtk_builder_connect_signals(builder, s);

	s->area = RUSPUZ_AREA(gtk_builder_get_object(builder, "area"));
	ruspuz_area_set_image(s->area, image);
	ruspuz_area_set_pieces(s->area, width, height);
	s->moves_label = GTK_LABEL(gtk_builder_get_object(builder, "moves"));
	s->moves_count = 0;
	s->timer = RUSPUZ_TIMER(gtk_builder_get_object(builder, "timer"));
	s->draw_pause_id = 0;
	s->menu_button =
	  GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "menubutton"));
	s->window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	gtk_window_set_icon_list(GTK_WINDOW(s->window), icon_list);

	static const GActionEntry actions[] = {
		{ "shuffle", shuffle_action },
		{ "close", close_action },
		{ "pause", toggle_pause_action },
		{ "cont", toggle_pause_action },
		{ "open", open_action },
		{ "about", about_action },
	};

	GActionMap *action_map = G_ACTION_MAP(s->window);
	g_action_map_add_action_entries(action_map,
	  actions, G_N_ELEMENTS(actions), s);

	s->pause =
	  G_SIMPLE_ACTION(g_action_map_lookup_action(action_map, "pause"));
	s->cont =
	  G_SIMPLE_ACTION(g_action_map_lookup_action(action_map, "cont"));
	g_simple_action_set_enabled(s->cont, FALSE);

	gtk_style_context_add_provider_for_screen(
	  gtk_widget_get_screen(s->window),
	  style,
	  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
	);

	g_object_unref(builder);
	gtk_widget_show_all(s->window);
	shuffle(s);
}

#define ICON(SIZE) RESOURCE("icon-" SIZE ".pnm")

static void startup(app, _)
	GtkApplication *app;
	gpointer _;
{
	//GtkBuilder *builder =
	  //gtk_builder_new_from_resource(RESOURCE("ruspuz.ui"));

	{ /* Icons */
		static const char *const resources[] = {
			ICON("small"),
			ICON("medium"),
			ICON("large"),
			NULL
		};
		for (const char *const *resource = resources;
		  *resource != NULL; resource++) {
			GdkPixbuf *pixbuf =
			  gdk_pixbuf_new_from_resource(*resource, NULL);
			if (pixbuf != NULL)
			  icon_list = g_list_prepend(icon_list, pixbuf);
		}
	}
	{ /* Style provider */
		GtkCssProvider *provider = gtk_css_provider_new();
		gtk_css_provider_load_from_resource(provider,
		  RESOURCE("main.css"));
		style = GTK_STYLE_PROVIDER(provider);
	}
#if 0
	{ /* Menu */
		gtk_application_set_menubar(app,
		  G_MENU_MODEL(gtk_builder_get_object(builder, "menubar")));
	}
	
	g_object_unref(builder);
#endif
}

static void move(area, x1, y1, x2, y2, s)
	RuspuzArea *area;
	gint x1, y1, x2, y2;
	struct state *s;
{
	gchar text[9 + 3 * sizeof s->moves_count];
	g_snprintf(text, sizeof text, "Moves: %d", ++s->moves_count);
	gtk_label_set_text(s->moves_label, text);
}

static gboolean solved(area, s)
	RuspuzArea *area;
	struct state *s;
{
	ruspuz_timer_stop(s->timer);
	ruspuz_area_set_display(area, RUSPUZ_AREA_ANIM);
	g_simple_action_set_enabled(s->pause, FALSE);
	return TRUE;
}

static gboolean key_press(window, evt, s)
	GtkWidget *window;
	GdkEventKey *evt;
	struct state *s;
{

	if (evt->type != GDK_KEY_PRESS) return FALSE;

	guint state = evt->state & GDK_MODIFIER_INTENT_DEFAULT_MOD_MASK;

	if (evt->keyval == GDK_KEY_q && state == GDK_CONTROL_MASK) {
		gtk_widget_destroy(window);
		return TRUE;
	} else if (evt->keyval == GDK_KEY_o && state == GDK_CONTROL_MASK) {
		dialog_open(s);
		return TRUE;
	} else if (!state &&
	  (evt->keyval == GDK_KEY_Alt_L || evt->keyval == GDK_KEY_Alt_R)) {
		/* Toggle */
		gtk_toggle_button_set_active(s->menu_button,
		  !gtk_toggle_button_get_active(s->menu_button));
		return TRUE;
	}

	return FALSE;
}

static gboolean area_key_press(area, evt, s)
	RuspuzArea *area;
	GdkEventKey *evt;
	struct state *s;
{
	if (evt->type != GDK_KEY_PRESS) return FALSE;

	guint state = evt->state & GDK_MODIFIER_INTENT_DEFAULT_MOD_MASK;

	if (evt->keyval == GDK_KEY_space && !state) {
		toggle_pause(s);
		return TRUE;
	} else if (evt->keyval == GDK_KEY_s && state == GDK_CONTROL_MASK) {
		shuffle(s);
		return TRUE;
	}

	return FALSE;
}

static void shuffle_action(action, param, s)
	GSimpleAction *action;
	GVariant *param;
	gpointer s;
{
	shuffle(s);
}

static void close_action(action, param, s)
	GSimpleAction *action;
	GVariant *param;
	gpointer s;
{
	gtk_widget_destroy(((struct state *)s)->window);
}

static void toggle_pause_action(action, param, s)
	GSimpleAction *action;
	GVariant *param;
	gpointer s;
{
	toggle_pause(s);
}

static gboolean draw_pause(area, cr, s)
	GtkWidget *area;
	cairo_t *cr;
	struct state *s;
{
	static const char text[] = "PAUSED";

	cairo_text_extents_t text_size;
	GtkAllocation widget_size;

	gtk_widget_get_allocation(area, &widget_size);
	cairo_set_font_size(cr, widget_size.height);
	cairo_text_extents(cr, text, &text_size);

	gdouble scale1 =
	  widget_size.height * (.8 * widget_size.height) / text_size.height;
	gdouble scale2 =
	  widget_size.height * (.8 * widget_size.width) / text_size.width; 

	cairo_set_font_size(cr, MIN(scale1, scale2));

	cairo_text_extents(cr, text, &text_size);

	cairo_move_to(cr,
	  (widget_size.width  - text_size.width)  / 2 - text_size.x_bearing,
	  (widget_size.height - text_size.height) / 2 - text_size.y_bearing);

	cairo_show_text(cr, text);

	return TRUE;
}

static void open_action(action, target, s)
	GSimpleAction *action;
	GVariant *target;
	gpointer s;
{
	dialog_open(s);
}

static void dialog_open(s)
	struct state *s;
{
	if (s->draw_pause_id == 0) toggle_pause(s);

	GtkWidget *dialog = gtk_file_chooser_dialog_new("Open - RusPuz",
	  GTK_WINDOW(s->window), GTK_FILE_CHOOSER_ACTION_OPEN,
	  "_Cancel", GTK_RESPONSE_CANCEL,
	  "_Open",   GTK_RESPONSE_ACCEPT,
	  NULL);

	GtkFileFilter *filter = gtk_file_filter_new();
	gtk_file_filter_add_pixbuf_formats(filter);

	gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filter);

	g_signal_connect(dialog, "response", G_CALLBACK(open), NULL);

	gtk_widget_show_all(dialog);
}

static void open(dialog, response, _)
	GtkFileChooserDialog *dialog;
	gint response;
	gpointer _;
{

	if (response != GTK_RESPONSE_ACCEPT) {
		gtk_widget_destroy(GTK_WIDGET(dialog));
		return;
	}

	gchar *filename =
	  gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
	gtk_widget_destroy(GTK_WIDGET(dialog));

	GError *err = NULL;
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file(filename, &err);

	g_free(filename);

	if (pixbuf == NULL) {
		GtkWidget *dialog = gtk_message_dialog_new(NULL,
		  0, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", err->message);
		g_error_free(err);
		g_signal_connect(dialog, "response",
		  G_CALLBACK(gtk_widget_destroy), NULL);

		gtk_widget_show_all(dialog);
		return;
	}

	display_window(GTK_APPLICATION(g_application_get_default()), pixbuf,
	  DEFAULT_WIDTH, DEFAULT_HEIGHT);

	g_object_unref(pixbuf);
}

static void shuffle(s)
	struct state *s;
{
	if (s->draw_pause_id != 0) toggle_pause(s);
	ruspuz_area_shuffle(s->area);
	ruspuz_area_set_display(s->area, RUSPUZ_AREA_TILES);
	ruspuz_timer_start(s->timer);
	s->moves_count = 0;
	gtk_label_set_text(s->moves_label, "Moves: 0");
}

static gboolean toggle_pause(s)
	struct state *s;
{
	if (s->draw_pause_id == 0) {
		/* Pause */

		if (ruspuz_area_get_display(s->area) != RUSPUZ_AREA_TILES)
		  return FALSE;

		s->draw_pause_id =
		  g_signal_connect(s->area, "draw", G_CALLBACK(draw_pause), s);
		s->click_pause_id = g_signal_connect_swapped(s->area,
		  "button-press-event", G_CALLBACK(toggle_pause), s);
		gtk_widget_queue_draw(GTK_WIDGET(s->area));
		ruspuz_area_set_display(s->area, RUSPUZ_AREA_IMAGE);
		ruspuz_timer_stop(s->timer);
		g_simple_action_set_enabled(s->pause, FALSE);
		g_simple_action_set_enabled(s->cont, TRUE);
	} else {
		/* Continue */

		g_signal_handler_disconnect(s->area, s->draw_pause_id);
		g_signal_handler_disconnect(s->area, s->click_pause_id);
		s->draw_pause_id = 0;
		gtk_widget_queue_draw(GTK_WIDGET(s->area));
		ruspuz_area_set_display(s->area, RUSPUZ_AREA_TILES);
		ruspuz_timer_continue(s->timer);
		g_simple_action_set_enabled(s->pause, TRUE);
		g_simple_action_set_enabled(s->cont, FALSE);
	}

	return TRUE;
}

static void about_action(action, variant, s)
	GSimpleAction *action;
	GVariant *variant;
	void *s;
{
	struct state *state = s;
	if (state->draw_pause_id == 0) toggle_pause(s);

	gtk_show_about_dialog(GTK_WINDOW(state->window),
		"logo", (GdkPixbuf *) g_list_last(icon_list)->data,
		"license-type", GTK_LICENSE_GPL_3_0,
		"program-name", "RusPuz",
		"copyright", "Copyright 2020 Wastl",
		"website", "https://gitlab.com/TheWastl/ruspuz",
		"comments", "A simple sliding puzzle",
	NULL);
}
