/* Copyright 2020 Wastl
 *
 * This file is part of RusPuz.
 *
 * RusPuz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RusPuz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ruspuztimer.h"

#include <glib/gprintf.h>

typedef struct {
	GTimer *timer;
	gboolean running;
	guint callback;
} RuspuzTimerPrivate;

enum {
	PROP0,
	PROP_RUNNING,
	N_PROPS
};

static GParamSpec *props[N_PROPS] = { NULL };

static void ruspuz_timer_class_init(RuspuzTimerClass *);
static void ruspuz_timer_init(RuspuzTimer *);

static gboolean ruspuz_timer_update(GtkWidget *, GdkFrameClock *, gpointer);

static void ruspuz_timer_dispose(GObject *);
static void ruspuz_timer_set_property(GObject *, guint, const GValue *,
  GParamSpec *);
static void ruspuz_timer_get_property(GObject *, guint, GValue *,
  GParamSpec *);

G_DEFINE_TYPE_WITH_PRIVATE(RuspuzTimer, ruspuz_timer, GTK_TYPE_LABEL)

static void ruspuz_timer_class_init(class)
	RuspuzTimerClass *class;
{
	GObjectClass *gobj_class = G_OBJECT_CLASS(class);

	gobj_class->dispose = ruspuz_timer_dispose;
	gobj_class->get_property = ruspuz_timer_get_property;
	gobj_class->set_property = ruspuz_timer_set_property;

	props[PROP_RUNNING] = g_param_spec_boolean("running",
	  "timer running?", "Whether the timer is running", TRUE,
	  G_PARAM_READWRITE);
}

static void ruspuz_timer_dispose(gobj)
	GObject *gobj;
{
	RuspuzTimerPrivate *priv =
	  ruspuz_timer_get_instance_private(RUSPUZ_TIMER(gobj));

	if (priv->timer != NULL) {
		g_timer_destroy(priv->timer);
		priv->timer = NULL;
	}

	if (priv->running) {
		priv->running = FALSE;
		gtk_widget_remove_tick_callback(GTK_WIDGET(gobj),
		  priv->callback);
	}

	G_OBJECT_CLASS(ruspuz_timer_parent_class)->dispose(gobj);
}

static void ruspuz_timer_set_property(gobj, prop, value, pspec)
	GObject *gobj;
	guint prop;
	const GValue *value;
	GParamSpec *pspec;
{
	RuspuzTimer *self = RUSPUZ_TIMER(gobj);
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	switch (prop) {
		case PROP_RUNNING:
			if (g_value_get_boolean(value)) {
				if (!priv->running)
				  ruspuz_timer_continue(self);
			} else {
				if (priv->running)
				  ruspuz_timer_stop(self);
			}
			break;
	}
}

static void ruspuz_timer_get_property(gobj, prop, value, pspec)
	GObject *gobj;
	guint prop;
	GValue *value;
	GParamSpec *pspec;
{
	RuspuzTimerPrivate *priv =
	  ruspuz_timer_get_instance_private(RUSPUZ_TIMER(gobj));

	switch (prop) {
		case PROP_RUNNING:
			g_value_set_boolean(value, priv->running);
			break;
	}
}

static void ruspuz_timer_init(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	priv->timer = NULL;
	priv->running = FALSE;
}

gboolean ruspuz_timer_running(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	return priv->running;
}

int ruspuz_timer_elapsed(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	if (priv->timer == NULL) return 0;
	else return (int) g_timer_elapsed(priv->timer, NULL);
}

void ruspuz_timer_start(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	if (priv->timer == NULL) priv->timer = g_timer_new();
	else g_timer_start(priv->timer);

	if (!priv->running) {
		priv->running = TRUE;
		priv->callback = gtk_widget_add_tick_callback(GTK_WIDGET(self),
		  ruspuz_timer_update, NULL, NULL);
		g_object_notify_by_pspec(G_OBJECT(self), props[PROP_RUNNING]);
	}
}

void ruspuz_timer_stop(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	g_return_if_fail(priv->running);

	g_timer_stop(priv->timer);
	priv->running = FALSE;
	gtk_widget_remove_tick_callback(GTK_WIDGET(self), priv->callback);
	g_object_notify_by_pspec(G_OBJECT(self), props[PROP_RUNNING]);
}

void ruspuz_timer_continue(self)
	RuspuzTimer *self;
{
	RuspuzTimerPrivate *priv = ruspuz_timer_get_instance_private(self);

	g_return_if_fail(!priv->running);

	if (priv->timer == NULL) priv->timer = g_timer_new();
	else g_timer_continue(priv->timer);
	priv->running = TRUE;
	priv->callback = gtk_widget_add_tick_callback(GTK_WIDGET(self),
	  ruspuz_timer_update, NULL, NULL);
	g_object_notify_by_pspec(G_OBJECT(self), props[PROP_RUNNING]);
}

gboolean ruspuz_timer_update(widget, clock, _)
	GtkWidget *widget;
	GdkFrameClock *clock;
	gpointer _;
{
	gint t = ruspuz_timer_elapsed(RUSPUZ_TIMER(widget));

	char str[10];
	g_snprintf(str, sizeof str, "%02d:%02d", t / 60, t % 60);
	gtk_label_set_text(GTK_LABEL(widget), str);

	return TRUE;
}
