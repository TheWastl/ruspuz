# RusPuz - simple sliding puzzle built with GTK+

## Build

You will need the GTK+ library and development files installed. On Debian
and Ubuntu, use:

	sudo apt-get install libgtk-3-dev

Then, on a Linux (and other \*nix) machine, type:

	make

This should build everything. The result is a standalone executable named
`ruspuz`.

For Windows, I dunno.

## Run

	./ruspuz [-h <height>] [-w <width>] [image files ...]

If no image files are given, numbers are used. You can also use `ruspuz -?`
to display option help.

## License

RusPuz is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RusPuz is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RusPuz.  If not, see <https://www.gnu.org/licenses/>.
